
ELC Disassembler
================

elcd is a small disassembler for Emacs Lisp byte-compiled files.


Usage
-----

Pass the byte-compiled file as an argument

    $ ./elcd my-byte-compiled-file.elc

elcd can also read from its standard input:

    $ echo '(nothing)' | ./elcd


Building
--------

List of *compilation* dependencies:

* gcc
* bison
* flex
* perl
* wget

To build elcd, just run `make`.


Implementation notes
--------------------

A part of elcd is generated from Emacs's source code. A small perl
script parses `src/bytecode.c` and compiles useful information to an
opcode table in `opcode.c`. See `mk_opcode.pl`.

elcd implements a reader for a large subset of Emacs Lisp. See `lex.l` and
`parse.y` for the lexer and grammar definitions.
