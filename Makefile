all: elcd

emacs-bytecode:
	@echo fetching emacs bytecode.c...
	@wget -nv -O $@ 'http://git.savannah.gnu.org/cgit/emacs.git/plain/src/bytecode.c'

opcode.c: mk_opcode.pl emacs-bytecode
	perl $< emacs-bytecode > $@

parse.c: parse.y
	bison -v --defines=parse.h -o parse.c parse.y

lex.c: lex.l parse.c
	flex -o lex.c --header-file=lex.h lex.l

elcd: elcd.c elcd.h parse.c lex.c opcode.c opcode.h
	gcc -Wall -g -o $@ lex.c parse.c elcd.c opcode.c

clean:
	rm -f lex.c lex.h parse.c parse.h opcode.c elcd emacs-bytecode
