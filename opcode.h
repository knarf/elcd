#ifndef _OPCODE_H_
#define _OPCODE_H_

typedef struct {
    char* name;
    unsigned int is_operand_encoded : 1;
    unsigned int is_operand_cst     : 1;
    unsigned int operand_value      : 8;
    unsigned int bytes_to_fetch     : 2; 
    unsigned int sub_value          : 8;
} opcode_t;

extern opcode_t opcode_table[256];

#endif /* _OPCODE_H_ */
