#ifndef _ELCD_H_
#define _ELCD_H_

#define DEFAULT_CAPACITY 32
#define E(...)                                                      \
    fprintf(stderr, "%s:%d  ", __FILE__, __LINE__),                 \
        fprintf(stderr, __VA_ARGS__), fputc('\n', stderr), exit(1)

typedef unsigned int uint;
typedef unsigned char uint8;

typedef struct {
    char* s;
    uint size;
} str_t;

typedef struct {
    struct lisp_t** t;
    uint capacity;
    uint size;
} symtab_t;

typedef struct cons_t {
    struct lisp_t *car, *cdr;
} cons_t;

typedef struct vec_t {
    struct lisp_t** t;
    uint capacity;
    uint size;
} vec_t;

typedef struct {
    struct lisp_t** t;
    uint capacity;
} reftab_t;

#define IS_STR_KIND(x)  ((x)->type & STR_T)
#define IS_SYM_KIND(x)  ((x)->type & SYM_T)
#define IS_LIST_KIND(x) ((x)->type & LIST_T)
#define IS_VEC_KIND(x)  ((x)->type & VEC_T)
#define IS_NUM_KIND(x)  ((x)->type & NUM_T)

typedef struct lisp_t {
    enum {
        BOOLVECTOR_T = 1,
        STR_T        = 1|2,

        SYM_T        = 4,

        NUM_T        = 8,

        STRINGPL_T = 16,
        HASH_T     = 32,
        CONS_T     = 16|32,

        BYTECODE_T   = 64,
        CHARTAB_T    = 128,
        VEC_T        = 64|128,

        LIST_T = 16|32|64|128,

    } type;
    union {
        char* sym;
        str_t* str;
        double num;
        cons_t cons;
        vec_t vec;
    };
} lisp_t;

typedef struct {
    symtab_t* st;
    reftab_t* rt;
    char* fn;
    char valid;
    vec_t* code;
} parse_context_t;

extern parse_context_t* current_context;
extern int opt_debug;

str_t* str_copy_token (char* s, size_t size);
lisp_t *object_new(int type);
lisp_t *cons_enclose_new(lisp_t *sym, lisp_t *o);
double read_float(char *s);
double read_int(uint base, char *s);
symtab_t *symtab_new(void);
lisp_t *symtab_get(symtab_t *st, char *s);
void symtab_free(symtab_t *st);
reftab_t *reftab_new(void);
lisp_t *reftab_get(reftab_t *rt, uint i);
void reftab_set(reftab_t *rt, uint i, lisp_t *o);
void reftab_free(reftab_t *rt);
vec_t *vec_new(void);
lisp_t *vec_get(vec_t *vec, uint i);
void vec_set(vec_t *vec, uint i, lisp_t *o);
void vec_add(vec_t *vec, lisp_t *o);
void vec_free(vec_t *vec);
parse_context_t *parse_file(char *fn);
void disassemble_file(parse_context_t *p);
vec_t *list_to_vec(lisp_t *c);
void string_to_bc(str_t *in, uint8 **out, uint *size);
void object_print_list(lisp_t *p);
void object_print(lisp_t *p);
void disassemble_code(lisp_t *arg, lisp_t *func, lisp_t *bc, lisp_t *cst);
void usage(char *prog);

#endif /* _ELCD_H_ */
