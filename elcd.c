#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h> // getopt
#include "elcd.h"
#include "parse.h"
#include "opcode.h"

/*
   XXX: memory allocation management idea:

   Alloc large chunk, store base pointer and used size
   - new object is the next pointer (base + size++)
   - free the whole chunk when done
*/

parse_context_t* current_context = NULL;
int opt_debug = 0;

str_t* str_copy_token (char* s, size_t size)
{
    str_t* p = malloc(sizeof(*p));
    p->s = malloc(sizeof(*p->s)*size);
    memcpy(p->s, s, size);
    p->size = size;
    return p;
}

lisp_t* object_new (int type)
{
    lisp_t* p = calloc(sizeof(*p), 1);
    p->type = type;
    return p;
}

lisp_t* cons_enclose_new (lisp_t* sym, lisp_t* o)
{
    lisp_t* p = object_new(CONS_T);
    p->cons.car = sym;
    p->cons.cdr = object_new(CONS_T);
    p->cons.cdr->cons.car = o;
    return p;
}


double read_float (char* s)
{
    return strtod(s, NULL);
}

double read_int (uint base, char* s)
{
    switch(base) {
    case 16:
        return strtol(s + 2, NULL, 16);
    case 10:
        return strtol(s, NULL, 10);
    case 8:
        return strtol(s[0] == '0' ? s : s + 2, NULL, 8);
    case 2:
        return strtol(s + 2, NULL, 2);
    }
    E("unknown base %u", base);
    return 0;
}



/* Symbol table */

symtab_t* symtab_new ()
{
    symtab_t* p = calloc(sizeof(*p), 1);
    p->capacity = DEFAULT_CAPACITY;
    p->t = calloc(sizeof(*p->t), p->capacity);
    return p;
}

lisp_t* symtab_get (symtab_t* st, char* s)
{
    uint i;
    for(i = 0; i < st->size; i++) {
        if(strcmp(s, st->t[i]->sym) == 0)
            return st->t[i];
    }

    if(st->size >= st->capacity) {
        st->capacity *= 2;
        st->t = realloc(st->t, sizeof(*st->t)*st->capacity);
    }

    lisp_t* new = object_new(SYM_T);
    new->sym = strdup(s);
    st->t[st->size++] = new;
    return new;
}

void symtab_free (symtab_t* st)
{
    free(st->t);
    free(st);
}

/* Reference table */

reftab_t* reftab_new ()
{
    reftab_t* p = calloc(sizeof(*p), 1);
    p->capacity = DEFAULT_CAPACITY;
    p->t = calloc(sizeof(*p->t), p->capacity);
    return p;
}

lisp_t* reftab_get (reftab_t* rt, uint i)
{
    if(i < rt->capacity)
        return rt->t[i];

    E("can't get ref %u", i);
    return NULL;
}

void reftab_set (reftab_t* rt, uint i, lisp_t* o)
{
    if(i >= rt->capacity) {
        rt->capacity = i+1;
        rt->t = realloc(rt->t, sizeof(*rt->t)*rt->capacity);
    }

    rt->t[i] = o;
}

void reftab_free (reftab_t* rt)
{
    free(rt->t);
    free(rt);
}

/* Vector type */

vec_t* vec_new ()
{
    vec_t* p = calloc(sizeof(*p), 1);
    p->capacity = DEFAULT_CAPACITY;
    p->t = calloc(sizeof(*p->t), p->capacity);
    return p;
}

lisp_t* vec_get (vec_t* vec, uint i)
{
    if(i < vec->size)
        return vec->t[i];

    E("can't get vec[%u]", i);
    return NULL;
}

void vec_set (vec_t* vec, uint i, lisp_t* o)
{
    if(i >= vec->size)
        E("can't set vec[%u]", i);

    vec->t[i] = o;
}

void vec_add (vec_t* vec, lisp_t* o)
{
    if(vec->size >= vec->capacity) {
        vec->capacity *= 2;
        vec->t = realloc(vec->t, sizeof(*vec->t)*vec->capacity);
    }

    vec->t[vec->size++] = o;
}

void vec_free (vec_t* vec)
{
    free(vec->t);
    free(vec);
}

parse_context_t* parse_file (char* fn)
{
    parse_context_t* p = calloc(sizeof(*p), 1);
    p->st = symtab_new();
    p->rt = reftab_new();
    p->code = vec_new();

    if(fn) {
        p->fn = fn;
        extern FILE* yyin;
        yyin = fopen(fn, "r");
    }
    current_context = p;
    p->valid = !yyparse();
    current_context = NULL;
    return p;
}

void disassemble_file (parse_context_t* p)
{
    uint i;
    lisp_t* Squote = symtab_get(p->st, "quote");
    lisp_t* Sdefalias = symtab_get(p->st, "defalias");
    lisp_t *args, *func, *bc, *cst;
    lisp_t* v[3] = {0};

    for(i = 0; i < p->code->size; i++) {
        /*
          v0          v1                            v2
          (defalias . ((quote . (funcname . nil)) . ((arglist . (bytecode . (const . nil))))))
        */


        v[0] = p->code->t[i];
        if(!(v[0] && v[0]->type == CONS_T && v[0]->cons.car->type == SYM_T
             && v[0]->cons.car == Sdefalias))
            continue;

        v[1] = v[0]->cons.cdr;
        if(!(v[1] && v[1]->type == CONS_T && v[1]->cons.car->type == CONS_T))
            continue;

        v[2] = v[1]->cons.cdr;
        if(!(v[2] && v[2]->type == CONS_T && v[2]->cons.car->type == BYTECODE_T))
            continue;

        if(v[2]->cons.car->cons.cdr->cons.car->type != STR_T)
            continue;

        func = v[1]->cons.car->cons.cdr->cons.car;
        args = v[2]->cons.car->cons.car;
        bc   = v[2]->cons.car->cons.cdr->cons.car;
        cst  = v[2]->cons.car->cons.cdr->cons.cdr->cons.car;

        disassemble_code(args, func, bc, cst);
    }
}

vec_t* list_to_vec (lisp_t* c)
{
    vec_t* v = vec_new();

    while(c && IS_LIST_KIND(c)) {
        vec_add(v, c->cons.car);
        c = c->cons.cdr;
    }
    return v;
}

/* FSM to parse subset of elisp string syntax used by bytecode */
void string_to_bc (str_t* in, uint8** out, uint* size)
{
    uint slen = in->size - 2;
    char* s = in->s + 1;
    char* end = s + slen;
    uint8* p = calloc(slen+1, 1);
    uint8 num = 0;
    int nbdigit = 0;
    enum {NUM, START, NORM} state = NORM;

    *out = p;

    for(; s != end; s++) {
    redo:
        switch(state) {
        case NORM:
            if(*s == '\\')
                state = START;
            else
                *p++ = *s;
            break;

        case START:
            if('0' <= *s&&*s <= '7') {
                num = *s - '0';
                nbdigit = 1;
                state = NUM;
            } else {
                state = NORM;
                switch(*s) {
                case 'a': *p++ = 007;  break;
                case 'b': *p++ = '\b'; break;
                case 'd': *p++ = 0177; break;
                case 'e': *p++ = 033;  break;
                case 'f': *p++ = '\f'; break;
                case 'n': *p++ = '\n'; break;
                case 'r': *p++ = '\r'; break;
                case 't': *p++ = '\t'; break;
                case 'v': *p++ = '\v'; break;
                case '"': *p++ = '"';  break;
                case '\\':*p++ = '\\'; break;
                }
            }
            break;

        case NUM:
            if('0' <= *s&&*s <= '7') {
                    num = num*8 + *s - '0';
                    nbdigit++;
                    if(nbdigit >= 3) {
                        state = NORM;
                        *p++ = num;
                    }
            } else {
                state = NORM;
                *p++ = num;
                goto redo;
            }
            break;
        }
    }

    *size = p - *out;
}

void object_print_list (lisp_t* p)
{
    while(p && IS_LIST_KIND(p)) {
        object_print(p->cons.car);
        if(p->cons.cdr && IS_LIST_KIND(p->cons.cdr))
            putchar(' ');
        p = p->cons.cdr;
    }
}

void object_print (lisp_t* p)
{
    switch(p->type) {
    case BOOLVECTOR_T:
        printf("#&");
        fwrite(p->str->s, p->str->size, 1, stdout);
        break;
    case STR_T:
        fwrite(p->str->s, p->str->size, 1, stdout);
        break;
    case SYM_T:
        printf("%s", p->sym);
        break;
    case NUM_T:
        printf("%f", p->num);
        break;
    case STRINGPL_T:
        printf("#(");
        object_print_list(p);
        printf (")");
        break;
    case HASH_T:
        printf("#s(");
        object_print_list(p);
        printf (")");
        break;
    case CONS_T:
        printf("(");
        object_print_list(p);
        printf (")");
        break;
    case BYTECODE_T:
        printf("#[");
        object_print_list(p);
        printf ("]");
        break;
    case CHARTAB_T:
        printf("#^[");
        object_print_list(p);
        printf ("]");
        break;
    case VEC_T:
        printf("[");
        object_print_list(p);
        printf ("]");
        break;
    default:
        E("unknown type");
    }
}

void disassemble_code(lisp_t* arg, lisp_t* func, lisp_t* bc, lisp_t* cst)
{
    uint8* buf;
    uint size;
    uint i;
    vec_t* vcst;

    string_to_bc(bc->str, &buf, &size);
    vcst = list_to_vec(cst);

    printf("%s:\n", func->sym);
    for(i = 0; i < size; i++) {
        const opcode_t op = opcode_table[buf[i]];

        printf("%3d %-20.20s", i, op.name ? op.name : "\033[31;1minvalid\033[m");

        // skip invalid op
        if(!op.name) {
            putchar('\n');
            continue;
        }

        // handle encoded operand
        if(op.is_operand_encoded) {
            if(op.is_operand_cst) {
                putchar(' ');
                object_print(vec_get(vcst, op.operand_value));
            } else
                printf(" %d", op.operand_value);
        }

        // or fetch operand
        else if(op.bytes_to_fetch > 0) {
            int v = buf[++i];
            if(op.bytes_to_fetch == 2)
                v += buf[++i] << 8;
            if(op.is_operand_cst) {
                putchar(' ');
                object_print(vec_get(vcst, v));
            } else
                printf(" %d", v);
        }
        putchar('\n');
    }
    putchar('\n');
    free(buf);
    free(vcst);
}

void usage (char* prog)
{
    fprintf(stderr, "Usage: %s [-d] [FILE]\n", prog);
}

int main (int argc, char** argv)
{
    int opt;
    while((opt = getopt(argc, argv, "d")) != -1) {
        switch(opt) {
        case 'd':
            opt_debug = 1;
            break;
        default:
            usage(argv[0]);
            return 1;
        }
    }
    parse_context_t* p;
    p = parse_file(optind < argc ? argv[optind] : NULL);

    char* name = p->fn ? p->fn : "stdin";

    printf("%s\n", name);

    if(p->valid) {
        disassemble_file(p);
    }

    fprintf(stderr, "%s %s\n", p->valid ? "ok" : "er", name);
    return !p->valid;
}
