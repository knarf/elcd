%{
#include <stdio.h>
#include <string.h>
#include "elcd.h"

void yyerror(char* s);
int yylex();
#define P(x, y) opt_debug ? printf("%20s <- %s\n", x, y) : 0

%}

%start prog

%union {
    lisp_t* o;
    struct {
        lisp_t* first;
        lisp_t* last;
    } list;
    int type;

    char* sym;
    str_t* str;
    double num;
    uint ref;
}

%type <o> object atom cons vector string symbol
%type <type> list_start vector_start
%type <list> list

%token <sym> SYM
%token <str> STRING
%token <num> NUM
%token LP;
%token RP;
%token LS;
%token RS;
%token BQUOTE
%token QUOTEFUNC
%token FILENAME
%token QUOTE
%token COMMA
%token DOT
%token LBOOLVEC
%token LCHARTAB
%token LHASHTAB
%token LSTRINGPL
%token LBYTECODE
%token <ref> REFGET
%token <ref> REFSET

%%

prog
: {
    P("prog", "empty");
}
| prog object {
    P("prog", "prog object");
    vec_add(current_context->code, $2);
}
;

object
: atom {
    P("object", "atom");
    $$ = $1;
}
| cons {
    P("object", "cons");
    $$ = $1;
}
| vector {
    P("object", "vector");
    $$ = $1;
}
| REFGET {
    P("object", "REFGET");
    $$ = reftab_get(current_context->rt, $1);
}
| REFSET object {
    P("object", "REFSET");
    reftab_set(current_context->rt, $1, $2);
    $$ = $2;
}
| QUOTE object {
    P("object", "QUOTE object");
    $$ = cons_enclose_new(symtab_get(current_context->st, "quote"), $2);
}
| BQUOTE object {
    P("object", "BQUOTE object");
    $$ = cons_enclose_new(symtab_get(current_context->st, "\\`"), $2);
}
| COMMA object {
    P("object", "COMMA object");
    $$ = cons_enclose_new(symtab_get(current_context->st, "\\,"), $2);
}
| QUOTEFUNC object {
    P("object", "QUOTEFUNC object");
    $$ = cons_enclose_new(symtab_get(current_context->st, "function"), $2);
}
;

string
: LBOOLVEC STRING {
    P("string", "LBOOLVEC STRING");
    $$ = object_new(STR_T);
    $$->str = $2;
}
| STRING {
    P("string", "STRING");
    $$ = object_new(STR_T);
    $$->str = $1;
}
;

symbol
: SYM {
    P("symbol", "SYM");
    $$ = symtab_get(current_context->st, $1);
}
| LP RP {
    P("symbol", "()");
    $$ = symtab_get(current_context->st, "nil");
}
;

atom
: symbol {
    P("atom", "symbol");
    $$ = $1;
}
| string {
    P("atom", "string");
    $$ = $1;
}
| NUM {
    P("atom", "NUM");
    $$ = object_new(NUM_T);
    $$->num = $1;
}
| FILENAME {
    P("atom", "FILENAME");
    if(current_context->fn) {
        $$ = object_new(STR_T);
        $$->str = malloc(sizeof(*$$->str));
        $$->str->s = current_context->fn;
        $$->str->size = strlen($$->str->s);
    } else {
        $$ = symtab_get(current_context->st, "nil");
    }
}
;

list
: object {
    P("list", "object");
    lisp_t* p = object_new(CONS_T);
    p->cons.car = $1;
    p->cons.cdr = NULL;
    $$.first = $$.last = p;
}
| list object {
    P("list", "list object");
    lisp_t* p = object_new(CONS_T);
    p->cons.car = $2;
    p->cons.cdr = NULL;
    $$ = $1;
    $$.last->cons.cdr = p;
    $$.last = p;
}
;

list_start
: LP {
    P("list_start", "(");
    $$ = CONS_T;
}
| LSTRINGPL {
    P("list_start", "LSTRINGPL");
    $$ = STRINGPL_T;
}
| LHASHTAB {
    P("list_start", "LHASHTAB");
    $$ = HASH_T;
}
;

cons
: list_start list RP {
    P("cons", "list_start list )");
    $$ = $2.first;
    $$->type = $1;
}
| list_start list DOT object RP {
    P("cons", "list_start list . object )");
    $2.last->cons.cdr = $4;
    $$ = $2.first;
    $$->type = $1;
}
;

vector_start
: LS {
    P("vector_start", "[");
    $$ = VEC_T;
}
| LBYTECODE {
    P("vector_start", "#[");
    $$ = BYTECODE_T;
}
| LCHARTAB {
    P("vector_start", "#^[");
    $$ = CHARTAB_T;
}
;

vector
: vector_start RS {
    P("vector", "vector_start ]");
    $$ = object_new($1);
    $$->cons.car = NULL;
    $$->cons.cdr = NULL;
}
| vector_start list RS {
    P("vector", "vector_start list ]");
    $$ = $2.first;
    $$->type = $1;
}
;

%%

extern int yylineno;
void yyerror(char* s)
{
    fprintf(stderr, "%d: %s\n", yylineno, s);
}

int yywrap (void)
{
    return(1);
}
