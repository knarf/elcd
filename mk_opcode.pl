#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw(max);

my $fn = $ARGV[0] || 'emacs-bytecode';
my %opcodes;
my $maxlen = 0;

sub add_multi_op {
    my ($name, $cst, $nb) = @_;
    if(not defined $nb) {
        $nb = 8;
    }

    for(my $i = 0; $i < $nb; $i++) {
        my $f = 0;
        my $s = $i;
        my $n = $name;

        if($i > 0) {
            $n .= "$i";
        }

        if($i == $nb-1) {
            $f = 2;
            $s = 0;
        }
        elsif($i == $nb-2) {
            $f = 1;
            $s = 0;
        }

        $opcodes{$n} = {
            'num' => $s,
            'fetch' => $f,
            'cst' => $cst
        };
    }
}


add_multi_op('varbind',   1);
add_multi_op('varref',    1);
add_multi_op('varset',    0);
add_multi_op('call',      0);
add_multi_op('unbind',    0);
add_multi_op('stack_ref', 0);
$opcodes{'goto'}                 = {'fetch' => 2};
$opcodes{'gotoifnil'}            = {'fetch' => 2};
$opcodes{'gotoifnonnil'}         = {'fetch' => 2};
$opcodes{'gotoifnilelsepop'}     = {'fetch' => 2};
$opcodes{'gotoifnonnilelsepop'}  = {'fetch' => 2};
$opcodes{'Rgoto'}                = {'fetch' => 1, 'sub' => 127};
$opcodes{'Rgotoifnil'}           = {'fetch' => 1, 'sub' => 128};
$opcodes{'Rgotoifnonnil'}        = {'fetch' => 1, 'sub' => 128};
$opcodes{'Rgotoifnilelsepop'}    = {'fetch' => 1, 'sub' => 128};
$opcodes{'Rgotoifnonnilelsepop'} = {'fetch' => 1, 'sub' => 128};
$opcodes{'listN'}                = {'fetch' => 1};
$opcodes{'concatN'}              = {'fetch' => 1};
$opcodes{'insertN'}              = {'fetch' => 1};
$opcodes{'discardN'}             = {'fetch' => 1};
$opcodes{'constant2'}            = {'cst' => 1, 'fetch' => 2};
$opcodes{'constant'}             = {'cst' => 1, 'num' => 0};
$opcodes{'stack_set'}            = {'fetch' => 1};
$opcodes{'stack_set2'}           = {'fetch' => 2};

# now, complete using bytecode.c
open my $f, '<', $fn or die "can't open $fn : $!";
while(<$f>) {
    next if !/^DEFINE \(B([^,]+),\s*(\d+)/;
    my ($name, $op) = ($1, oct($2));

    $maxlen = max(length($name), $maxlen);

    if(defined $opcodes{$name}{'op'}) {
        die "op $name has multiple code";
    }
    $opcodes{$name}{'op'} = $op;
}
close $f;

# check every defined op has a code
for(keys %opcodes) {
    if(not exists $opcodes{$_}{op}) {
        die "op $_ doesn't have a code";
    }
}


my $maxcode = $opcodes{'constant'}{'op'};
# hack for remaining `constant' opcode
for(my $i = 1; $maxcode + $i <= 0xff; $i++) {
    my $code = $maxcode + $i;
    my $n = "const$i";
    $opcodes{$n} = {
        'cst' => 1,
        'op'  => $code,
        'num' => $i,
    };
}

print qq(#include "opcode.h"\n);
print qq(opcode_t opcode_table[256] = {\n);

$maxlen += 2;
my @snames = sort {$opcodes{$a}{'op'} <=> $opcodes{$b}{'op'}} keys %opcodes;
my $fmt = '    [0x%02X] = { %-'.$maxlen.'.'.$maxlen.'s, %d, %d, %2d, %d, %3d },'."\n";
for my $n (@snames) {
    my $code = $opcodes{$n}{'op'};
    my $is_encoded = 0;
    my $is_cst = 0;
    my $op_val = 0;
    my $fetch = 0;
    my $subval = 0;

    if(defined $opcodes{$n}{'num'}) {
        $is_encoded = 1;
        $op_val = $opcodes{$n}{'num'};
    }

    if(defined $opcodes{$n}{'fetch'}) {
        $fetch = $opcodes{$n}{'fetch'};
    }

    if(defined $opcodes{$n}{'sub'}) {
        $subval = $opcodes{$n}{'sub'};
    }

    if(defined $opcodes{$n}{'cst'}) {
        $is_cst = 1;
    }

    printf $fmt, $code, '"'.$n.'"', $is_encoded, $is_cst, $op_val, $fetch, $subval;
}

print "};\n";
