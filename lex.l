%{

#include <stdio.h>
#include "elcd.h"
#include "parse.h"
#define YY_USER_ACTION opt_debug ? printf("TOK <%.20s>\n", yytext) : 0;
%}

/* one input file */
%option noyywrap
%option yylineno

LP       "("
RP       ")"
LS       "["
RS       "]"

LBYTECODE    "#["
LSTRINGPL    "#("
LHASHTAB     "#s("

LCHARTAB     "#^""^"?"["
LBOOLVEC     "#&"

DOT          "."

SKIP         "#@"[0-9]+" "[^\x1f]*\x1f
SHEB         "#!".*
COMMENT      ";".*

FILENAME     "#$"

QUOTE        "'"
BQUOTE       "`"
COMMA        ","

STRING       \"(\\.|[^\\"])*\"


DNUM     "-"?(([1-9][0-9]+)|[0-9])
HNUM     "#x"[0-9a-fA-F]+
BNUM     "#b"[01]+
ONUM     ("0"[0-7]+)|("#o"[0-7]+)
FNUM     "-"?(([0-9]+"."[0-9]*)|("."[0-9]+))

SYMFC  (\\(.|\n)|[^\\\"\'\;\(\)\[\]\#\`\,\.[:space:]0-9])
SYMC   (\\(.|\n)|[^\\\"\'\;\(\)\[\]\#\`\,\.[:space:]])
SYMNAME [0-9]*{SYMFC}{SYMC}*
SYM   ("#:"?{SYMNAME}+)|"##"

QUOTEFUNC    "#'"

REFSET "#"[0-9]+"="
REFGET "#"[0-9]+"#"

SPACE [[:space:]]

%%

{SPACE}   ;
{COMMENT} ;
{SHEB}    ;
{SKIP}    ;

{RS}        {return RS;}
{LS}        {return LS;}
{RP}        {return RP;}
{LP}        {return LP;}
{QUOTEFUNC} {return QUOTEFUNC;}
{COMMA}     {return COMMA;}
{BQUOTE}    {return BQUOTE;}
{QUOTE}     {return QUOTE;}
{FILENAME}  {return FILENAME;}
{DOT}       {return DOT;}

{LBOOLVEC}  {return LBOOLVEC;}
{LCHARTAB}  {return LCHARTAB;}
{LHASHTAB}  {return LHASHTAB;}
{LSTRINGPL} {return LSTRINGPL;}
{LBYTECODE} {return LBYTECODE;}

{REFGET} {
    yylval.ref = atoi(yytext+1);
    return REFGET;
}

{REFSET} {
    yylval.ref = atoi(yytext+1);
    return REFSET;
}

{SYM} {
    yylval.sym = yytext;
    return SYM;
}

{FNUM} {
    yylval.num = read_float(yytext);
    return NUM;
}

{ONUM} {
    yylval.num = read_int(8, yytext);
    return NUM;
}

{BNUM} {
    yylval.num = read_int(2, yytext);
    return NUM;
}

{HNUM} {
    yylval.num = read_int(16, yytext);
    return NUM;
}

{DNUM} {
    yylval.num = read_int(10, yytext);
    return NUM;
}

{STRING} {
    yylval.str = str_copy_token(yytext, yyleng);
    return STRING;
}

. E("err");

%%
